# Promise
ES6的Promise实现，兼容低版本浏览器，有更多详细信息便查调试追踪问题。
性能比ES6的Promise原生实现要快些。

ES6原生Promise中有一个Bug，当Promise成功返回带有then方法的对象时，则无法正常运作：

```
new Promise(function(resolve){
	resolve({
		then:function(){ console.log("call then"); }
	});
}).then(function(result){
	console.log("success"); //无法正常输出
})
```


有评论者提问，为什么里面还依赖原生promise，原因如下：
这个可以用setTimeout代替哈，但是setTimeout的性能不高，所以我还需要找时间考虑下用什么来替换。  
如果需要不依赖promise，可以自行修改下源码里，里面只有一个地方使用了原生promise来替换setTimeout的作用，自行修改成setTimeout应该也是OK的，我猜想性能不会差太远。
才刚发布，就有评论提问了，后面我也会想其它办法改进这个问题。
有疑问可以在评论处提问。